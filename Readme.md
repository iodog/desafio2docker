## Resolução do Desafio 2 de Docker do Curso [Full Cycle 3.0](https://fullcycle.com.br/)
&nbsp;
&nbsp;
### URL do repositório: <https://gitlab.com/iodog/desafio2docker>
---
&nbsp;
&nbsp;
### Para rodar a aplicação:

```
sudo docker-compose up -d
```
&nbsp;
&nbsp;

#### Abrir o navegador em <http://localhost:8080/>