const express = require("express");
const app = express();
const port = 3000;
const config = {
  host: "db",
  user: "root",
  password: "root",
  database: "nodedb",
};
const mysql = require("mysql2");
const connection = mysql.createConnection(config);

const sqlInsert = `INSERT INTO people(name) values('Rafael Godoi Orbolato')`;
const sqlQuery = `SELECT * FROM people`;

app.get("/", (req, res) => {
  const connection = mysql.createConnection(config);
  connection.query(sqlInsert, function (err, result) {
    if (err) throw err;
    console.log("1 record inserted");
  });

  connection.query(sqlQuery, function (err, result, fields) {
    let nomes = [];
    if (err) throw err;
    console.log(result);
    let saida = '\
<!DOCTYPE html>\
<html lang="pt-BR">\
<head>\
<meta charset="UTF-8">\
<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">\
<title>Página Principal</title>\
</head>\
<body>\
    <h1>Full Cycle Rocks!</h1>\
    <h2>Nomes:</h2>\
    <ul>';
    result.forEach((p) => {
      saida += "<li>" + p.name + "</li>";
    });
    saida += '</ul>\
</body>\
</html>';
    console.log(saida);
    res.send(saida);
  });
  connection.end();
});

app.listen(port, () => {
  console.log("Rodando na porta " + port);
});
